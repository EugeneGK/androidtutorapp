package com.eugineer.androidtutorapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ListFragment extends Fragment {

    public interface ListInterractionListener {
        void onTextClicked();
    }
    ListInterractionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListInterractionListener) {
            mListener = (ListInterractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ListInterractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.getView()
        .findViewById(R.id.button2)
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTextClicked();
            }
        });
    }

    public void setTextView (String text) {
        Button button = (Button) this.getView().findViewById(R.id.button2);
        button.setText(text);
    }

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }
}
