package com.eugineer.androidtutorapp;

import android.app.Application;

import com.eugineer.androidtutorapp.dagger.components.AppComponent;
import com.eugineer.androidtutorapp.dagger.components.DaggerAppComponent;

/**
 * Created by eugineer on 24/04/17.
 */

public class App extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.create();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
