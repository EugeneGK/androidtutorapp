package com.eugineer.androidtutorapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DisplayMessageActivity
        extends AppCompatActivity
        implements ListFragment.ListInterractionListener  {

    @Override
    public void onTextClicked() {
        TextView textView = (TextView) findViewById(R.id.textView);
        String message = textView.getText().toString();
        message += 'c';
        textView.setText(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(message);



        InsertableFragment insertableFragment = new InsertableFragment();

        getSupportFragmentManager()
            .beginTransaction()
            .add(R.id.fragment_container, insertableFragment)
            .commit();

        insertableFragment = new InsertableFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, insertableFragment)
                .commit();

        ListFragment lf = new ListFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_list, lf)
                .commit();
    }
}
