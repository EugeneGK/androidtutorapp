package com.eugineer.androidtutorapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.eugineer.androidtutorapp.dagger.providers.DatabaseHelper;
import com.eugineer.androidtutorapp.dagger.providers.NetworkUtils;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.eugineer.androidtutorapp.MESSAGE";

    @Inject
    DatabaseHelper  databaseHelper;
    @Inject
    NetworkUtils    networkUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*
        databaseHelper = App.getAppComponent().getDatabaseHelper();
        networkUtils = App.getAppComponent().getNetworkUtils();
*/
        App.getAppComponent().injectMainActivity(this);
    }

    public void sendMessage (View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);

        EditText exitText = (EditText) findViewById(R.id.editText);
        String message = exitText.getText().toString();

        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent);
    }
}
