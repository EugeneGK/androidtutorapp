package com.eugineer.androidtutorapp.dagger.components;

import com.eugineer.androidtutorapp.MainActivity;
import com.eugineer.androidtutorapp.dagger.modules.DatabaseModule;
import com.eugineer.androidtutorapp.dagger.modules.NetworkModule;
import com.eugineer.androidtutorapp.dagger.providers.DatabaseHelper;
import com.eugineer.androidtutorapp.dagger.providers.NetworkUtils;

import dagger.Component;

/**
 * Created by eugineer on 24/04/17.
 */

@Component(modules = {DatabaseModule.class, NetworkModule.class})
public interface AppComponent {

    NetworkUtils getNetworkUtils();
    DatabaseHelper getDatabaseHelper();

    //имя может быть любым, важна сигнатура
    void injectMainActivity(MainActivity mainActivity);

}
