package com.eugineer.androidtutorapp.dagger.modules;

import com.eugineer.androidtutorapp.dagger.providers.HttpClient;
import com.eugineer.androidtutorapp.dagger.providers.NetworkUtils;

import dagger.Module;
import dagger.Provides;

/**
 * Created by eugineer on 24/04/17.
 */

@Module
public class NetworkModule {

    @Provides
    NetworkUtils provideNetworkUtils(HttpClient httpClient) {
        return new NetworkUtils(httpClient);
    }

    @Provides
    HttpClient provideHttpClient(){
        return new HttpClient();
    }
}
