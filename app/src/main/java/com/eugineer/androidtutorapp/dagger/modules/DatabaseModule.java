package com.eugineer.androidtutorapp.dagger.modules;

import com.eugineer.androidtutorapp.dagger.providers.DatabaseHelper;
import dagger.Module;
import dagger.Provides;

/**
 * Created by eugineer on 24/04/17.
 */

@Module
public class DatabaseModule {

    @Provides
    DatabaseHelper provideDatabaseHelper() {
        return new DatabaseHelper();
    }
}
