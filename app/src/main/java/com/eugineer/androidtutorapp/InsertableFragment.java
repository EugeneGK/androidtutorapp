package com.eugineer.androidtutorapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class InsertableFragment extends Fragment {
    public static int fragmentCounter = 0;

    public InsertableFragment() {
        // Required empty public constructor
        ++fragmentCounter;
        fragmentNumber = fragmentCounter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_insertable, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView textView = (TextView) this.getView().findViewById(R.id.textView);
        textView.append(String.valueOf(fragmentCounter));
    }

    private int fragmentNumber = 0;
}
